import "./assets/main.css";

import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";

import ElementPlus from "element-plus";
import "element-plus/dist/index.css";

axios.defaults.baseURL = "http://10.214.241.122:8000/api";
axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
axios.defaults.headers.common["Access-Control-Allow-Credentials"] = "true";

// 从 localstorage 获取 token
const token = localStorage.getItem("token");
axios.defaults.headers.common["Authorization"] = token;

const app = createApp(App);

app.use(router);
app.use(ElementPlus);

app.mount("#app");
