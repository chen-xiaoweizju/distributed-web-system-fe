import axios from "axios";
import { ElMessage } from "element-plus";

export const getTickets = async (data) => {
	try {
		const res = await axios.post("/tickets/normal/", data);
		return res.data.data;
	} catch (error) {
		if (error.response && error.response.status === 500) {
			ElMessage.error("服务器内部错误，请联系管理员");
		} else if (error.response) {
			ElMessage.error(error.response.data.message);
		} else {
			ElMessage.error("请求失败，请检查网络连接");
		}
		return null;
	}
};