import axios from "axios";
import { ElMessage } from "element-plus";
import router from "@/router";

export const login = async (data) => {
	try {
		const res = await axios.post("/user/login", data);
		localStorage.setItem("token", res.data.token);
		location.reload();
		router.push("/home");
	} catch (error) {
		if (error.response.status == 500)
			ElMessage.error("服务器内部错误，请联系管理员");
		else ElMessage.error(error.response.data.message);
		if(error.response.status === 401) router.push("/login");
	}
};
export const register_ = async (data) => {
	try {
		const res = await axios.post("", data);
		if(res.data.status === 200) ElMessage.success("创建账号成功");
		else ElMessage.error(res.data.message);
	} catch (error) {
		if (error.response.status == 500)
			ElMessage.error("服务器内部错误，请联系管理员");
		else ElMessage.error(error.response.data.message);
		if(error.response.status === 401) router.push("/login");

	}
};

export const register = async (data) => {
	var a=1;
	axios.post("/user/register", {
		email: data.email,
		password: data.key
	})
		.then(response => {
			// 请求成功处理逻辑
			if (response.status === 201) {
				console.log("注册成功");
				localStorage.setItem("token", response.data.data.token);
			}
			a=1;
		})
		.catch(error => {
			// 请求错误处理逻辑
			if (!error.response) {
				console.error("无法连接到服务器");
				ElMessage.error("请求失败");
				return;
			}
			switch (error.response.status) {
			case 409:
				console.error("用户已存在，需要重新注册");
				ElMessage.error("用户已存在，需要重新注册");
				break;
			case 500:
				console.error("服务器错误，请稍后再试");
				// 这里进行弹窗提示
				ElMessage.error("服务器错误，请稍后再试");
				break;
			default:
				console.error("发生未知错误");
				ElMessage.error("发生未知错误");
			}
			a=2;
		});
	return a;
};
export const verifyToken = async (data) => {
	var b=1;
	axios
		.post("/user/verify", {
			token: data
		})
		.then(response => {
			if (response.status === 200) {
				console.log("验证成功");
			}
			b=1;
		})
		.catch(error => {
			if (!error.response) {
				console.error("无法连接到服务器");
				return;
			}
			switch (error.response.status) {
			case 408:
				console.error("验证失败");
				break;
			case 500:
				console.error("服务器错误，请稍后再试");
				break;
			default:
				console.error("发生未知错误");
			}
			b=2;
		});
	return b;
};
