import { createRouter, createWebHistory } from "vue-router";
import flightInfoView from "../views/FlightInfo.vue";
const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: "/",
			redirect: "/home"  // 重定向到首页
		},
		{
			path: "/home",
			name: "home",
			component: flightInfoView
		},
		{
			path: "/device",
			name: "device",
			meta: { requiresAuth: true },//设置需要鉴权
			component: () => import("../views/DeviceView.vue")
		},
		{
			path: "/login",
			name: "login",
			// route level code-splitting
			// this generates a separate chunk (About.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import("../views/LoginView.vue")
		},
		{
			path: "/register",
			name: "register",
			// route level code-splitting
			// this generates a separate chunk (About.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import("../views/RegisterView.vue")
		},
		{
			path: "/verify/:token",
			name: "verify",
			component: () => import("../views/VerificationView.vue")
		}
	]
});

router.beforeEach(async (to, from, next) => {
	// 检查路由是否需要鉴权
	const token = localStorage.getItem("token");
	// 使用 GET 请求验证 token
	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (token) {
			next(); // Token 有效，继续
		} else {
			console.error("Token 不存在");
			next("/login"); // 验证失败，阻止
		}
	} else if (token && to.path === "/login") {
		next("/home"); // 如果已登录但访问登录页，自动跳转到首页
	} else {
		next(); // 不需要鉴权的路由直接放行
	}
});
  
export default router;
