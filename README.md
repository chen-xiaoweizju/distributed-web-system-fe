# git 规约
## commit message
`type: message` （英文冒号，冒号后空一格）
- `feat` 功能
- `fix` bug修复
- `chore` 其他
- `refactor` 重构
- `init` 首次提交
示例
`feat: 登录页面`

## 分支设计
### 主分支
- `main`
- `qa`
- `dev`

### 开发分支
- `feat/branch_name`
- `fix/branch_name`
- `chore/branch_name`
- `refactor/branch_name`

## 开发流程
1. 拉取最新的 dev 分支
2. 在 dev 分支上使用 `git branch branch_name` 创建新分支
3. 在 dev 分支上使用 `git checkout branch_name` 切换到对应分支
4. 在该分支上进行开发、commit 和 push
5. 前往 gitlab 发起 merge request，source branch 为开发分支，目标分支为 dev 分支，选择具有合并分支权限的同事作为 code reviewer

## 若无法提交
1. 检查是否通过 eslint 检查，使用 npm run lint:fix 进行自动修复
2. 检查 commit msg 是否符合要求
3. 检查是否有在该分支进行 commit 的权限

# 前端规约
## 项目结构
- views 页面
- components 组件
- assets 静态资源
- router 路由
- utils 工具
- api 存放 axios 请求

## 编码规范
- 使用箭头函数，而不是 `function`，例：`const example = () => {}`
- 变量使用 camel_case 命名法，例： `myVar`
- 所有 axios 请求放在 api 目录下，再在对应文件中导入，不要在视图文件中编写 axios 请求
- 所有错误信息处理和依赖请求的路由跳转放在 axios 请求中，不要在视图文件中进行错误信息处理和路由跳转，若有不懂请参考 api/user.js 文件中的写法
- 在视图文件中编写 css 样式时，务必使用 `<style scoped></style>`
- 如需更改 element-plus 组件样式，请在 `base.css` 文件中编写 css 样式

## 代码质量控制
- 本项目使用 `eslint` 进行代码质量控制，请勿修改根目录下 `.eslintrc.json` 文件
